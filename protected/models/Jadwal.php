<?php

/**
 * This is the model class for table "jadwal".
 *
 * The followings are the available columns in table 'jadwal':
 * @property string $hari
 * @property string $kelas
 * @property integer $no_ruang
 * @property string $jam_kuliah
 * @property string $nama_matakuliah
 * @property integer $nip
 */
class Jadwal extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'jadwal';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('hari, kelas, no_ruang, jam_kuliah, nama_matakuliah, nip', 'required'),
			array('no_ruang, nip', 'numerical', 'integerOnly'=>true),
			array('hari, nama_matakuliah', 'length', 'max'=>20),
			array('kelas', 'length', 'max'=>4),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('hari, kelas, no_ruang, jam_kuliah, nama_matakuliah, nip', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'hari' => 'Hari',
			'kelas' => 'Kelas',
			'no_ruang' => 'No Ruang',
			'jam_kuliah' => 'Jam Kuliah',
			'nama_matakuliah' => 'Nama Matakuliah',
			'nip' => 'Nip',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('hari',$this->hari,true);
		$criteria->compare('kelas',$this->kelas,true);
		$criteria->compare('no_ruang',$this->no_ruang);
		$criteria->compare('jam_kuliah',$this->jam_kuliah,true);
		$criteria->compare('nama_matakuliah',$this->nama_matakuliah,true);
		$criteria->compare('nip',$this->nip);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Jadwal the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
