<?php

/**
 * This is the model class for table "kreasimhs".
 *
 * The followings are the available columns in table 'kreasimhs':
 * @property integer $no_kreasi
 * @property string $nama
 * @property string $judul_kreasi
 * @property string $jenis_kreasi
 * @property string $tanggal
 * @property string $kreasi
 */
class Kreasimhs extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'kreasimhs';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nama, judul_kreasi, jenis_kreasi, tanggal, kreasi', 'required'),
			array('nama, judul_kreasi, jenis_kreasi', 'length', 'max'=>50),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('no_kreasi, nama, judul_kreasi, jenis_kreasi, tanggal, kreasi', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'no_kreasi' => 'No Kreasi',
			'nama' => 'Nama',
			'judul_kreasi' => 'Judul Kreasi',
			'jenis_kreasi' => 'Jenis Kreasi',
			'tanggal' => 'Tanggal',
			'kreasi' => 'Kreasi',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('no_kreasi',$this->no_kreasi);
		$criteria->compare('nama',$this->nama,true);
		$criteria->compare('judul_kreasi',$this->judul_kreasi,true);
		$criteria->compare('jenis_kreasi',$this->jenis_kreasi,true);
		$criteria->compare('tanggal',$this->tanggal,true);
		$criteria->compare('kreasi',$this->kreasi,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Kreasimhs the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
