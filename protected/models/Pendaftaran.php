<?php

/**
 * This is the model class for table "pendaftaran".
 *
 * The followings are the available columns in table 'pendaftaran':
 * @property integer $no_pendaftaran
 * @property string $nama
 * @property string $alamat_sekarang
 * @property string $kota
 * @property integer $kode_pos
 * @property string $ttl
 * @property string $agama
 * @property string $jenis_kelamin
 * @property string $kewarganegaraan
 * @property string $status_pegawai
 * @property string $alamat_kerja
 * @property string $telepon_kantor
 * @property string $no_telepon
 * @property string $pendidikan_terakhir
 * @property string $nama_sekolah
 * @property string $alamat_sekolah
 * @property string $jurusan_sekolah
 * @property integer $tahun_lulus
 * @property string $nama_ayah
 * @property string $nama_ibu
 * @property string $alamat_ortu
 * @property integer $gaji_ortu
 * @property string $jurusan
 * @property string $jenis
 * @property string $jenjang
 */
class Pendaftaran extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pendaftaran';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nama, alamat_sekarang, kota, kode_pos, ttl, agama, jenis_kelamin, kewarganegaraan, status_pegawai, alamat_kerja, telepon_kantor, no_telepon, pendidikan_terakhir, nama_sekolah, alamat_sekolah, jurusan_sekolah, tahun_lulus, nama_ayah, nama_ibu, alamat_ortu, gaji_ortu, jurusan, jenis, jenjang', 'required'),
			array('kode_pos, tahun_lulus, gaji_ortu', 'numerical', 'integerOnly'=>true),
			array('nama, alamat_sekarang, alamat_kerja, alamat_sekolah, alamat_ortu', 'length', 'max'=>100),
			array('kota, kewarganegaraan, status_pegawai, pendidikan_terakhir, nama_sekolah, jurusan_sekolah, nama_ayah, nama_ibu', 'length', 'max'=>50),
			array('agama, jenis_kelamin, jenjang', 'length', 'max'=>10),
			array('telepon_kantor, no_telepon', 'length', 'max'=>13),
			array('jurusan, jenis', 'length', 'max'=>30),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('no_pendaftaran, nama, alamat_sekarang, kota, kode_pos, ttl, agama, jenis_kelamin, kewarganegaraan, status_pegawai, alamat_kerja, telepon_kantor, no_telepon, pendidikan_terakhir, nama_sekolah, alamat_sekolah, jurusan_sekolah, tahun_lulus, nama_ayah, nama_ibu, alamat_ortu, gaji_ortu, jurusan, jenis, jenjang', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'no_pendaftaran' => 'No Pendaftaran',
			'nama' => 'Nama',
			'alamat_sekarang' => 'Alamat Sekarang',
			'kota' => 'Kota',
			'kode_pos' => 'Kode Pos',
			'ttl' => 'Ttl',
			'agama' => 'Agama',
			'jenis_kelamin' => 'Jenis Kelamin',
			'kewarganegaraan' => 'Kewarganegaraan',
			'status_pegawai' => 'Status Pegawai',
			'alamat_kerja' => 'Alamat Kerja',
			'telepon_kantor' => 'Telepon Kantor',
			'no_telepon' => 'No Telepon',
			'pendidikan_terakhir' => 'Pendidikan Terakhir',
			'nama_sekolah' => 'Nama Sekolah',
			'alamat_sekolah' => 'Alamat Sekolah',
			'jurusan_sekolah' => 'Jurusan Sekolah',
			'tahun_lulus' => 'Tahun Lulus',
			'nama_ayah' => 'Nama Ayah',
			'nama_ibu' => 'Nama Ibu',
			'alamat_ortu' => 'Alamat Ortu',
			'gaji_ortu' => 'Gaji Ortu',
			'jurusan' => 'Jurusan',
			'jenis' => 'Jenis',
			'jenjang' => 'Jenjang',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('no_pendaftaran',$this->no_pendaftaran);
		$criteria->compare('nama',$this->nama,true);
		$criteria->compare('alamat_sekarang',$this->alamat_sekarang,true);
		$criteria->compare('kota',$this->kota,true);
		$criteria->compare('kode_pos',$this->kode_pos);
		$criteria->compare('ttl',$this->ttl,true);
		$criteria->compare('agama',$this->agama,true);
		$criteria->compare('jenis_kelamin',$this->jenis_kelamin,true);
		$criteria->compare('kewarganegaraan',$this->kewarganegaraan,true);
		$criteria->compare('status_pegawai',$this->status_pegawai,true);
		$criteria->compare('alamat_kerja',$this->alamat_kerja,true);
		$criteria->compare('telepon_kantor',$this->telepon_kantor,true);
		$criteria->compare('no_telepon',$this->no_telepon,true);
		$criteria->compare('pendidikan_terakhir',$this->pendidikan_terakhir,true);
		$criteria->compare('nama_sekolah',$this->nama_sekolah,true);
		$criteria->compare('alamat_sekolah',$this->alamat_sekolah,true);
		$criteria->compare('jurusan_sekolah',$this->jurusan_sekolah,true);
		$criteria->compare('tahun_lulus',$this->tahun_lulus);
		$criteria->compare('nama_ayah',$this->nama_ayah,true);
		$criteria->compare('nama_ibu',$this->nama_ibu,true);
		$criteria->compare('alamat_ortu',$this->alamat_ortu,true);
		$criteria->compare('gaji_ortu',$this->gaji_ortu);
		$criteria->compare('jurusan',$this->jurusan,true);
		$criteria->compare('jenis',$this->jenis,true);
		$criteria->compare('jenjang',$this->jenjang,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Pendaftaran the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
