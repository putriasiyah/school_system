<?php
/* @var $this PendaftaranController */
/* @var $model Pendaftaran */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'pendaftaran-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'nama'); ?>
		<?php echo $form->textField($model,'nama',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'nama'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'alamat_sekarang'); ?>
		<?php echo $form->textField($model,'alamat_sekarang',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'alamat_sekarang'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'kota'); ?>
		<?php echo $form->textField($model,'kota',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'kota'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'kode_pos'); ?>
		<?php echo $form->textField($model,'kode_pos'); ?>
		<?php echo $form->error($model,'kode_pos'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ttl'); ?>
		<?php echo $form->textField($model,'ttl'); ?>
		<?php echo $form->error($model,'ttl'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'agama'); ?>
		<?php echo $form->textField($model,'agama',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'agama'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'jenis_kelamin'); ?>
		<?php echo $form->textField($model,'jenis_kelamin',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'jenis_kelamin'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'kewarganegaraan'); ?>
		<?php echo $form->textField($model,'kewarganegaraan',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'kewarganegaraan'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'status_pegawai'); ?>
		<?php echo $form->textField($model,'status_pegawai',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'status_pegawai'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'alamat_kerja'); ?>
		<?php echo $form->textField($model,'alamat_kerja',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'alamat_kerja'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'telepon_kantor'); ?>
		<?php echo $form->textField($model,'telepon_kantor',array('size'=>13,'maxlength'=>13)); ?>
		<?php echo $form->error($model,'telepon_kantor'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'no_telepon'); ?>
		<?php echo $form->textField($model,'no_telepon',array('size'=>13,'maxlength'=>13)); ?>
		<?php echo $form->error($model,'no_telepon'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'pendidikan_terakhir'); ?>
		<?php echo $form->textField($model,'pendidikan_terakhir',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'pendidikan_terakhir'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'nama_sekolah'); ?>
		<?php echo $form->textField($model,'nama_sekolah',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'nama_sekolah'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'alamat_sekolah'); ?>
		<?php echo $form->textField($model,'alamat_sekolah',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'alamat_sekolah'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'jurusan_sekolah'); ?>
		<?php echo $form->textField($model,'jurusan_sekolah',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'jurusan_sekolah'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tahun_lulus'); ?>
		<?php echo $form->textField($model,'tahun_lulus'); ?>
		<?php echo $form->error($model,'tahun_lulus'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'nama_ayah'); ?>
		<?php echo $form->textField($model,'nama_ayah',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'nama_ayah'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'nama_ibu'); ?>
		<?php echo $form->textField($model,'nama_ibu',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'nama_ibu'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'alamat_ortu'); ?>
		<?php echo $form->textField($model,'alamat_ortu',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'alamat_ortu'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'gaji_ortu'); ?>
		<?php echo $form->textField($model,'gaji_ortu'); ?>
		<?php echo $form->error($model,'gaji_ortu'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'jurusan'); ?>
		<?php echo $form->textField($model,'jurusan',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'jurusan'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'jenis'); ?>
		<?php echo $form->textField($model,'jenis',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'jenis'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'jenjang'); ?>
		<?php echo $form->textField($model,'jenjang',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'jenjang'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->