<?php
/* @var $this PendaftaranController */
/* @var $data Pendaftaran */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('no_pendaftaran')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->no_pendaftaran), array('view', 'id'=>$data->no_pendaftaran)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nama')); ?>:</b>
	<?php echo CHtml::encode($data->nama); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('alamat_sekarang')); ?>:</b>
	<?php echo CHtml::encode($data->alamat_sekarang); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kota')); ?>:</b>
	<?php echo CHtml::encode($data->kota); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kode_pos')); ?>:</b>
	<?php echo CHtml::encode($data->kode_pos); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ttl')); ?>:</b>
	<?php echo CHtml::encode($data->ttl); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('agama')); ?>:</b>
	<?php echo CHtml::encode($data->agama); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('jenis_kelamin')); ?>:</b>
	<?php echo CHtml::encode($data->jenis_kelamin); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kewarganegaraan')); ?>:</b>
	<?php echo CHtml::encode($data->kewarganegaraan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status_pegawai')); ?>:</b>
	<?php echo CHtml::encode($data->status_pegawai); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('alamat_kerja')); ?>:</b>
	<?php echo CHtml::encode($data->alamat_kerja); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('telepon_kantor')); ?>:</b>
	<?php echo CHtml::encode($data->telepon_kantor); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('no_telepon')); ?>:</b>
	<?php echo CHtml::encode($data->no_telepon); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pendidikan_terakhir')); ?>:</b>
	<?php echo CHtml::encode($data->pendidikan_terakhir); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nama_sekolah')); ?>:</b>
	<?php echo CHtml::encode($data->nama_sekolah); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('alamat_sekolah')); ?>:</b>
	<?php echo CHtml::encode($data->alamat_sekolah); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jurusan_sekolah')); ?>:</b>
	<?php echo CHtml::encode($data->jurusan_sekolah); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tahun_lulus')); ?>:</b>
	<?php echo CHtml::encode($data->tahun_lulus); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nama_ayah')); ?>:</b>
	<?php echo CHtml::encode($data->nama_ayah); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nama_ibu')); ?>:</b>
	<?php echo CHtml::encode($data->nama_ibu); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('alamat_ortu')); ?>:</b>
	<?php echo CHtml::encode($data->alamat_ortu); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('gaji_ortu')); ?>:</b>
	<?php echo CHtml::encode($data->gaji_ortu); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jurusan')); ?>:</b>
	<?php echo CHtml::encode($data->jurusan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jenis')); ?>:</b>
	<?php echo CHtml::encode($data->jenis); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jenjang')); ?>:</b>
	<?php echo CHtml::encode($data->jenjang); ?>
	<br />

	*/ ?>

</div>