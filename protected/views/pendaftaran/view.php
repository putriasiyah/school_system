<?php
/* @var $this PendaftaranController */
/* @var $model Pendaftaran */

$this->breadcrumbs=array(
	'Pendaftarans'=>array('index'),
	$model->no_pendaftaran,
);

$this->menu=array(
	array('label'=>'List Pendaftaran', 'url'=>array('index')),
	array('label'=>'Create Pendaftaran', 'url'=>array('create')),
	array('label'=>'Update Pendaftaran', 'url'=>array('update', 'id'=>$model->no_pendaftaran)),
	array('label'=>'Delete Pendaftaran', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->no_pendaftaran),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Pendaftaran', 'url'=>array('admin')),
);
?>

<h1>View Pendaftaran #<?php echo $model->no_pendaftaran; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'no_pendaftaran',
		'nama',
		'alamat_sekarang',
		'kota',
		'kode_pos',
		'ttl',
		'agama',
		'jenis_kelamin',
		'kewarganegaraan',
		'status_pegawai',
		'alamat_kerja',
		'telepon_kantor',
		'no_telepon',
		'pendidikan_terakhir',
		'nama_sekolah',
		'alamat_sekolah',
		'jurusan_sekolah',
		'tahun_lulus',
		'nama_ayah',
		'nama_ibu',
		'alamat_ortu',
		'gaji_ortu',
		'jurusan',
		'jenis',
		'jenjang',
	),
)); ?>
