<?php
/* @var $this HasilSelesksiController */
/* @var $data HasilSelesksi */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('no_pendaftaran')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->no_pendaftaran), array('view', 'id'=>$data->no_pendaftaran)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nama')); ?>:</b>
	<?php echo CHtml::encode($data->nama); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('keterangan')); ?>:</b>
	<?php echo CHtml::encode($data->keterangan); ?>
	<br />


</div>