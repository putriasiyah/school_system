<?php
/* @var $this HasilSelesksiController */
/* @var $model HasilSelesksi */

$this->breadcrumbs=array(
	'Hasil Selesksis'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List HasilSelesksi', 'url'=>array('index')),
	array('label'=>'Manage HasilSelesksi', 'url'=>array('admin')),
);
?>

<h1>Create HasilSelesksi</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>