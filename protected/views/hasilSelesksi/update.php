<?php
/* @var $this HasilSelesksiController */
/* @var $model HasilSelesksi */

$this->breadcrumbs=array(
	'Hasil Selesksis'=>array('index'),
	$model->no_pendaftaran=>array('view','id'=>$model->no_pendaftaran),
	'Update',
);

$this->menu=array(
	array('label'=>'List HasilSelesksi', 'url'=>array('index')),
	array('label'=>'Create HasilSelesksi', 'url'=>array('create')),
	array('label'=>'View HasilSelesksi', 'url'=>array('view', 'id'=>$model->no_pendaftaran)),
	array('label'=>'Manage HasilSelesksi', 'url'=>array('admin')),
);
?>

<h1>Update HasilSelesksi <?php echo $model->no_pendaftaran; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>