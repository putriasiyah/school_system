<?php
/* @var $this HasilSelesksiController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Hasil Selesksis',
);

$this->menu=array(
	array('label'=>'Create HasilSelesksi', 'url'=>array('create')),
	array('label'=>'Manage HasilSelesksi', 'url'=>array('admin')),
);
?>

<h1>Hasil Selesksis</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
