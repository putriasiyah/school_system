<?php
/* @var $this HasilSelesksiController */
/* @var $model HasilSelesksi */

$this->breadcrumbs=array(
	'Hasil Selesksis'=>array('index'),
	$model->no_pendaftaran,
);

$this->menu=array(
	array('label'=>'List HasilSelesksi', 'url'=>array('index')),
	array('label'=>'Create HasilSelesksi', 'url'=>array('create')),
	array('label'=>'Update HasilSelesksi', 'url'=>array('update', 'id'=>$model->no_pendaftaran)),
	array('label'=>'Delete HasilSelesksi', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->no_pendaftaran),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage HasilSelesksi', 'url'=>array('admin')),
);
?>

<h1>View HasilSelesksi #<?php echo $model->no_pendaftaran; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'no_pendaftaran',
		'nama',
		'keterangan',
	),
)); ?>
