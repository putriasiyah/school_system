<?php
/* @var $this KreasimhsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Kreasimhs',
);

$this->menu=array(
	array('label'=>'Create Kreasimhs', 'url'=>array('create')),
	array('label'=>'Manage Kreasimhs', 'url'=>array('admin')),
);
?>

<h1>Kreasimhs</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
