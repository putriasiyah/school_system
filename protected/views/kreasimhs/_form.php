<?php
/* @var $this KreasimhsController */
/* @var $model Kreasimhs */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'kreasimhs-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'nama'); ?>
		<?php echo $form->textField($model,'nama',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'nama'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'judul_kreasi'); ?>
		<?php echo $form->TextArea($model,'judul_kreasi',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'judul_kreasi'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'jenis_kreasi'); ?>
		<?php echo $form->textField($model,'jenis_kreasi',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'jenis_kreasi'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tanggal'); ?>
		<?php echo $form->textField($model,'tanggal'); ?>
		<?php echo $form->error($model,'tanggal'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'kreasi'); ?>
		<?php echo $form->textField($model,'kreasi',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'kreasi'); ?>
	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'browse' : 'Save'); ?>
	</div>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Post Kreasi' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->