<?php
/* @var $this KreasimhsController */
/* @var $model Kreasimhs */

$this->breadcrumbs=array(
	'Kreasimhs'=>array('index'),
	$model->no_kreasi,
);

$this->menu=array(
	array('label'=>'List Kreasimhs', 'url'=>array('index')),
	array('label'=>'Create Kreasimhs', 'url'=>array('create')),
	array('label'=>'Update Kreasimhs', 'url'=>array('update', 'id'=>$model->no_kreasi)),
	array('label'=>'Delete Kreasimhs', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->no_kreasi),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Kreasimhs', 'url'=>array('admin')),
);
?>

<h1>View Kreasimhs #<?php echo $model->no_kreasi; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'no_kreasi',
		'nama',
		'judul_kreasi',
		'jenis_kreasi',
		'tanggal',
		'kreasi',
	),
)); ?>
