<?php
/* @var $this KreasimhsController */
/* @var $model Kreasimhs */

$this->breadcrumbs=array(
	'kreasimahasiswa'=>array('index'),
	'postkreasi',
);

$this->menu=array(
	array('label'=>'List Kreasimhs', 'url'=>array('index')),
	array('label'=>'Manage Kreasimhs', 'url'=>array('admin')),
);
?>

<h1>Kreasi Mahasiswa</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>