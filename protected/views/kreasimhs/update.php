<?php
/* @var $this KreasimhsController */
/* @var $model Kreasimhs */

$this->breadcrumbs=array(
	'Kreasimhs'=>array('index'),
	$model->no_kreasi=>array('view','id'=>$model->no_kreasi),
	'Update',
);

$this->menu=array(
	array('label'=>'List Kreasimhs', 'url'=>array('index')),
	array('label'=>'Create Kreasimhs', 'url'=>array('create')),
	array('label'=>'View Kreasimhs', 'url'=>array('view', 'id'=>$model->no_kreasi)),
	array('label'=>'Manage Kreasimhs', 'url'=>array('admin')),
);
?>

<h1>Update Kreasimhs <?php echo $model->no_kreasi; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>