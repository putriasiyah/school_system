<?php
/* @var $this KreasimhsController */
/* @var $data Kreasimhs */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('no_kreasi')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->no_kreasi), array('view', 'id'=>$data->no_kreasi)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nama')); ?>:</b>
	<?php echo CHtml::encode($data->nama); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('judul_kreasi')); ?>:</b>
	<?php echo CHtml::encode($data->judul_kreasi); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jenis_kreasi')); ?>:</b>
	<?php echo CHtml::encode($data->jenis_kreasi); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tanggal')); ?>:</b>
	<?php echo CHtml::encode($data->tanggal); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kreasi')); ?>:</b>
	<?php echo CHtml::encode($data->kreasi); ?>
	<br />


</div>