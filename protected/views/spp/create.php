<?php
/* @var $this SppController */
/* @var $model Spp */

$this->breadcrumbs=array(
	'Spp'=>array('index'),
	'Bayarspp',
);

$this->menu=array(
	array('label'=>'List Spp', 'url'=>array('index')),
	array('label'=>'Manage Spp', 'url'=>array('admin')),
);
?>

<h1>Pembayaran SPP</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>