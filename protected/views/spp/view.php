<?php
/* @var $this SppController */
/* @var $model Spp */

$this->breadcrumbs=array(
	'Spps'=>array('index'),
	$model->nrp,
);

$this->menu=array(
	array('label'=>'List Spp', 'url'=>array('index')),
	array('label'=>'Create Spp', 'url'=>array('create')),
	array('label'=>'Update Spp', 'url'=>array('update', 'id'=>$model->nrp)),
	array('label'=>'Delete Spp', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->nrp),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Spp', 'url'=>array('admin')),
);
?>

<h1>View Spp #<?php echo $model->nrp; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'nrp',
		'nama',
		'semester',
		'nominal',
		'keterangan',
	),
)); ?>
