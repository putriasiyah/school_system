<?php
/* @var $this SppController */
/* @var $model Spp */

$this->breadcrumbs=array(
	'Spps'=>array('index'),
	$model->nrp=>array('view','id'=>$model->nrp),
	'Update',
);

$this->menu=array(
	array('label'=>'List Spp', 'url'=>array('index')),
	array('label'=>'Create Spp', 'url'=>array('create')),
	array('label'=>'View Spp', 'url'=>array('view', 'id'=>$model->nrp)),
	array('label'=>'Manage Spp', 'url'=>array('admin')),
);
?>

<h1>Update Spp <?php echo $model->nrp; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>